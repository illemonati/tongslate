"use client";

import Link from "next/link";
import { useState } from "react";

export const NavBar = () => {
    const [mobileMenuOpen, setMobileMenuOpen] = useState(false);

    const handleMobileMenuToggle = () => {
        setMobileMenuOpen((s) => !s);
    };

    return (
        <>
            <nav className="bg-gray-800">
                <div className="mx-auto max-w-7xl px-2 sm:px-6 lg:px-8">
                    <div className="relative flex h-16 items-center justify-between">
                        <div className="absolute inset-y-0 left-0 flex items-center sm:hidden">
                            <button
                                type="button"
                                onClick={() => {
                                    handleMobileMenuToggle();
                                }}
                                className="relative inline-flex items-center justify-center rounded-md p-2 text-gray-400 hover:bg-gray-700 hover:text-white focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white"
                                aria-controls="mobile-menu"
                                aria-expanded="false"
                            >
                                <span className="absolute -inset-0.5"></span>

                                <svg
                                    className="block h-6 w-6"
                                    fill="none"
                                    viewBox="0 0 24 24"
                                    stroke-width="1.5"
                                    stroke="currentColor"
                                    aria-hidden="true"
                                >
                                    <path
                                        stroke-linecap="round"
                                        stroke-linejoin="round"
                                        d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5"
                                    />
                                </svg>

                                <svg
                                    className="hidden h-6 w-6"
                                    fill="none"
                                    viewBox="0 0 24 24"
                                    stroke-width="1.5"
                                    stroke="currentColor"
                                    aria-hidden="true"
                                >
                                    <path
                                        stroke-linecap="round"
                                        stroke-linejoin="round"
                                        d="M6 18L18 6M6 6l12 12"
                                    />
                                </svg>
                            </button>
                        </div>
                        <div className="flex flex-1 items-center justify-center sm:items-stretch sm:justify-start">
                            <div className="flex flex-shrink-0 items-center">
                                <p className="text-sky-400 font-bold text-xl">
                                    Tongslate
                                </p>
                            </div>
                            <div className="hidden sm:ml-6 sm:block">
                                <div className="flex space-x-4">
                                    <p className="text-lime-400 rounded-md px-3 py-2 text-sm font-medium">
                                        Translations as if they are from a
                                        highschool foreign language student
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className="absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0 hidden sm:ml-6 sm:block">
                            <div className="flex space-x-4">
                                <Link
                                    href="https://www.manythings.org/anki/"
                                    target="_blank"
                                >
                                    <p className="text-gray-300 rounded-md px-3 py-2 text-sm font-medium hover:text-white hover:bg-gray-700">
                                        Data
                                    </p>
                                </Link>
                                <Link
                                    href="https://gitlab.com/illemonati/tongslate"
                                    target="_blank"
                                >
                                    <p className="text-gray-300 rounded-md px-3 py-2 text-sm font-medium hover:text-white hover:bg-gray-700">
                                        Source
                                    </p>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>

                {mobileMenuOpen && (
                    <div className="sm:hidden">
                        <div className="space-y-1 px-2 pb-3 pt-2">
                            <p className="text-lime-400 rounded-md px-3 py-2 text-sm font-medium">
                                Translations as if they are from a highschool
                                foreign language student
                            </p>
                            <Link
                                href="https://www.manythings.org/anki/"
                                target="_blank"
                            >
                                <p
                                    className="text-gray-300 hover:bg-gray-700 hover:text-white block rounded-md px-3 py-2 text-base font-medium"
                                    aria-current="page"
                                >
                                    Data
                                </p>
                            </Link>
                            <Link
                                href="https://gitlab.com/illemonati/tongslate"
                                target="_blank"
                            >
                                <p className="text-gray-300 hover:bg-gray-700 hover:text-white block rounded-md px-3 py-2 text-base font-medium">
                                    Source
                                </p>
                            </Link>
                        </div>
                    </div>
                )}
            </nav>
        </>
    );
};
