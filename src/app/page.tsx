"use client";

import { ChangeEvent, useEffect, useState } from "react";
import {
    getListOfFromLanguages,
    getListOfToLanguages,
    languageList,
} from "./languge-pairs";
import { MODEL_SERVER, getTranslation } from "./configs";

export default function Home() {
    const [inputContent, setInputConent] = useState("");
    const [translatedContent, setTranslatedContent] = useState("");

    const possibleFromLanguages = getListOfFromLanguages();

    const [fromLanguage, setFromLanguage] = useState(possibleFromLanguages[0]);
    const [possibleToLanguages, setPossibleToLanguages] = useState(
        getListOfToLanguages(fromLanguage)
    );
    const [toLanguage, setToLanguage] = useState(possibleToLanguages[0]);

    const handleFromClick = (newFromLanguage: string) => () => {
        setFromLanguage(newFromLanguage);
    };

    const handleToClick = (newToLanguage: string) => () => {
        setToLanguage(() => {
            translate(inputContent, newToLanguage).then();
            return newToLanguage;
        });
    };

    const translate = async (original: string, newToLanguage?: string) => {
        original = original.replace(/\s+/g, " ").trim();
        if (original.length === 0) return;
        const resp = await fetch("/model-api", {
            method: "POST",
            body: JSON.stringify({
                fromLanguage,
                toLanguage: newToLanguage || toLanguage,
                original,
            }),
            headers: {
                "Content-Type": "application/json",
            },
        });

        const translation = await resp.json();
        setTranslatedContent(() => translation);
    };

    const handleInputChange = (e: ChangeEvent<HTMLTextAreaElement>) => {
        setInputConent(e.target.value);
        console.log(e);
        translate(e.target.value).then();
    };

    useEffect(() => {
        console.log(fromLanguage);
        setPossibleToLanguages(() => getListOfToLanguages(fromLanguage));
    }, [fromLanguage]);

    useEffect(() => {
        console.log(possibleToLanguages);
        setToLanguage(() => possibleToLanguages[0]);
    }, [possibleToLanguages]);

    return (
        <main className="flex flex-col items-center justify-between p-4 flex-1">
            <div className="max-w-5xl w-full items-center justify-between text-sm flex gap-8 md:flex-row flex-col md:pt-32 ">
                <div className="h-56 md:w-2/4 w-full">
                    <div className="h-1/6 w-full mb-3 flex gap-4 ml-2">
                        {possibleFromLanguages.map((possibleFromLanguage) => {
                            return (
                                <p
                                    key={possibleFromLanguage}
                                    className={
                                        possibleFromLanguage === fromLanguage
                                            ? "text-xl cursor-pointer border-b-2 border-sky-800"
                                            : "text-xl cursor-pointer"
                                    }
                                    onClick={handleFromClick(
                                        possibleFromLanguage
                                    )}
                                >
                                    {languageList[possibleFromLanguage]}
                                </p>
                            );
                        })}
                    </div>
                    <textarea
                        value={inputContent}
                        onChange={(e) => handleInputChange(e)}
                        className="h-5/6 w-full resize-none text-xl p-2 ring-1 ring-slate-900/10 shadow-sm rounded-md"
                    ></textarea>
                </div>
                <div className="h-56 md:w-2/4 w-full">
                    <div className="h-1/6 w-full mb-3 flex gap-4 pr-2 justify-end">
                        {possibleToLanguages.map((possibleToLanguage) => {
                            return (
                                <p
                                    key={possibleToLanguage}
                                    onClick={handleToClick(possibleToLanguage)}
                                    className={
                                        possibleToLanguage === toLanguage
                                            ? "text-xl cursor-pointer border-b-2 border-sky-800"
                                            : "text-xl cursor-pointer"
                                    }
                                >
                                    {languageList[possibleToLanguage]}
                                </p>
                            );
                        })}
                    </div>
                    <textarea
                        disabled
                        value={translatedContent}
                        className="h-5/6 w-full resize-none text-xl p-2 ring-1 ring-slate-900/10 shadow-sm rounded-md"
                    ></textarea>
                </div>
            </div>
            <div className="absolute bottom-4 text-center text-sm text-teal-800 p-2">
                <p>
                    Try simple sentences (not compound), varying punctuation and
                    word choice.
                </p>
            </div>
        </main>
    );
}
