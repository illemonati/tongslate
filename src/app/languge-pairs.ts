interface LanguagePair {
    from: string;
    to: string;
}

export const languagePairsAvaliable: LanguagePair[] = [
    { from: "eng", to: "spa" },
    { from: "eng", to: "fra" },
];

export const getListOfFromLanguages = () => {
    return Array.from(new Set(languagePairsAvaliable.map((pair) => pair.from)));
};

export const getListOfToLanguages = (from: string) => {
    return Array.from(
        new Set(
            languagePairsAvaliable
                .filter((pair) => pair.from === from)
                .map((pair) => pair.to)
        )
    );
};

export const languageList: { [key: string]: string } = {
    eng: "English",
    spa: "Spanish",
    fra: "French",
};
