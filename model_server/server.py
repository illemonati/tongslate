import tensorflow as tf
from bottle import post, run, app, response, request
import pickle
import numpy as np
from bottle_cors_plugin import cors_plugin
import re

models_path = './serving_models'
max_input_length = 80
max_output_length = 80

language_models = {
    'eng-spa': {
    },
    'eng-fra': {

    }
}


def str_to_tokens(sentence, lang1_dict):
    words = sentence.lower().split()
    tokens_list = [lang1_dict[word] for word in words]
    return tf.keras.preprocessing.sequence.pad_sequences([tokens_list], maxlen=max_input_length, padding='post')


def preprocess_line(line):
    return ' '.join(re.findall(r"[\w']+|[.,!?;。，！？；]", line))


def tokenize_lines(lines):
    tokenizer = tf.keras.preprocessing.text.Tokenizer(
        filters='!"#$%&()*+-/:;<=>@[\\]^_`{|}~\t\n')
    tokenizer.fit_on_texts(lines)
    tokenized_lines = tokenizer.texts_to_sequences(lines)
    word_dict = tokenizer.word_index
    num_tokens = len(word_dict) + 1
    return tokenized_lines, word_dict, num_tokens


for language_pair, values in language_models.items():
    base_path = f"{models_path}/{language_pair}"
    values['encoder'] = tf.keras.models.load_model(
        f"{base_path}/encoder.h5")
    values['decoder'] = tf.keras.models.load_model(
        f"{base_path}/decoder.h5")

    # values['encoder'].save(f"{base_path}/encoder.h5")
    # values['decoder'].save(f"{base_path}/decoder.h5")

    values['encoder'].compile()
    values['decoder'].compile()

    with open(f"{base_path}/lang1.pickle", 'rb') as file:
        values['lang1_dict'] = pickle.load(file)
    with open(f"{base_path}/lang2.pickle", 'rb') as file:
        values['lang2_dict'] = pickle.load(file)
    values['lang2_dict_reversed'] = {
        v: k for k, v in values['lang2_dict'].items()}


@post('/translate/<language_pair>')
def translate(language_pair):

    print(language_pair)

    if language_pair not in language_models:
        response.status = 400
        return {'err': 'invalid language'}

    values = language_models[language_pair]

    original = request.json['original']
    print(original)
    states_values = values['encoder'].predict(str_to_tokens(
        preprocess_line(original), values['lang1_dict']))
    empty_target_seq = np.zeros((1, 1))
    empty_target_seq[0, 0] = values['lang2_dict']['start']
    stop_condition = False
    decoded_translation = ''
    while not stop_condition:
        dec_outputs, h, c = values['decoder'].predict(
            [empty_target_seq, *states_values], verbose=0)
        sampled_word_index = np.argmax(dec_outputs[0, -1, :])
        sampled_word = values['lang2_dict_reversed'][
            sampled_word_index] if sampled_word_index in values['lang2_dict_reversed'] else '<unknown>'

        if sampled_word == 'end' or len(decoded_translation.split()) > max_output_length:
            stop_condition = True
        else:
            decoded_translation += f" {sampled_word}"

        empty_target_seq[0, 0] = sampled_word_index
        states_values = [h, c]

    print(decoded_translation)

    return {'translated': decoded_translation}


app = app()
app.install(cors_plugin('*'))

run(port=12293, debug=True, reloader=False)
