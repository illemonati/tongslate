import { NextRequest, NextResponse } from "next/server";
import { getTranslation } from "../configs";

export async function POST(request: NextRequest) {
    const { fromLanguage, toLanguage, original } = await request.json();
    return NextResponse.json(
        await getTranslation(fromLanguage, toLanguage, original)
    );
}
