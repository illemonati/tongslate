FROM nikolaik/python-nodejs:python3.10-nodejs18 as python-build
WORKDIR /opt/app

RUN python3 -m venv /opt/app/venv
COPY ./requirements.txt .
COPY ./tensorflow-2.8.0-cp310-cp310-linux_x86_64.whl .
# no avx
RUN /opt/app/venv/bin/python -m pip install ./tensorflow-2.8.0-cp310-cp310-linux_x86_64.whl
RUN /opt/app/venv/bin/python -m pip install -r ./requirements.txt
RUN /opt/app/venv/bin/python -m pip install protobuf==3.20.*

FROM nikolaik/python-nodejs:python3.10-nodejs18  as node-build
WORKDIR /opt/app
COPY . .
ENV NODE_ENV=container
RUN yarn install
RUN yarn build

FROM nikolaik/python-nodejs:python3.10-nodejs18


COPY --from=python-build /opt/app/venv/ /opt/app/venv/
COPY --from=node-build /opt/app/.next/ /opt/app/.next/
COPY --from=node-build /opt/app/node_modules/ /opt/app/node_modules/

WORKDIR /opt/app
COPY . .
ENV NODE_ENV=container

EXPOSE 11797
CMD yarn start & /opt/app/venv/bin/python model_server/server.py