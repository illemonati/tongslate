export const MODEL_SERVER =
    process.env.MODEL_SERVER || "http://127.0.0.1:12293";

export const getTranslation = async (
    fromLang: string,
    toLang: string,
    original: string
) => {
    const res = await fetch(`${MODEL_SERVER}/translate/${fromLang}-${toLang}`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            original,
        }),
    });

    const translated = (await res.json()).translated;
    return translated;
};
